from laboratoire import *


def gerer_arrivee(labo): 
    nom = input("nom")
    bureau = input("bureau")
    enregistrer_arrivee(labo, nom, bureau)

def gerer_depart(labo):
    nom = input("nom")
    enregistrer_depart(labo, nom)

def gerer_bureau(labo):
    nom = input("nom")
    bureau = input("nouveau bureau")
    modifier_bureau (labo,nom,bureau)

def gerer_nom(labo):
    nom = input("nom")
    modifier_nom(labo, nom)
    

def information_membre(labo):
    nom = input("nom")
    afficher_membre (labo, nom)

def information_bureau(labo):
    nom = input("nom")
    afficher_bureau (labo, nom)

def all_personnels(labo):
    lister_labo(labo)

def occupation_des_bureaux(labo):
        bureau_list = occupation_bureaux(labo)
        for bureau, nom_labo in bureau_list.items():
            print(bureau, ":")
            for nom in nom_labo :
                print("-",nom)


def afficher_menu():
    print ("1: Enregistrer arrivée")
    print ("2: Enregistrer départ")
    print ("3: Modifier le bureau")
    print ("4: Modifier le nom")
    print ("5: Afficher un membre")
    print ("6: Afficher un bureau")
    print ("7: Afficher l'ensemble des membres et leurs bureaux")
    print ("8: Afficher l'occupation des bureaux")
    print ("0: Quitter")

def choix_utilisateur():
    return int(input("Quel est votre choix ? "))

def traiter (choix,labo):       #L'idée est de le remplacer par un menu
    if choix == 1 :
        gerer_arrivee (labo)
    elif choix == 2 :
        gerer_depart (labo)
    elif choix == 3 :
        gerer_bureau (labo)
    elif choix == 4 :
        gerer_nom(labo)
    elif choix == 5 :
        information_membre(labo)
    elif choix == 6 :
        information_bureau(labo)
    elif choix == 7 : 
        all_personnels(labo)
    elif choix == 8 :
        occupation_des_bureaux(labo)


def main ():     #initialiser dans cette fonction les objets
    # mp = Menu() 
    # ajouter (mp, "arrivée")
    labo = Laboratoire()
    choix = None
                                    #def gerer_arrivee(labo=labo):
    while choix != 0 :                  # dans def main(): def action.guerrearrivee():
        afficher_menu()                                     #gereerarrivee(labo) le labo=laboratoire qui est définit dans main
        choix = choix_utilisateur()   
        traiter (choix, labo)
        print("labo",labo)

if __name__=="__main__":
    main()