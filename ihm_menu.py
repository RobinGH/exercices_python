from laboratoire import *
from menu import *

def main():
    labo = Laboratoire()

    # définir les fonctons du menu

    def gerer_arrivee(labo=labo):
        nom = input("nom")
        bureau = input("bureau")
        enregistrer_arrivee(labo, nom, bureau)

    def gerer_depart(labo=labo):
        nom = input("nom")
        enregistrer_depart(labo,nom)

    def gerer_bureau(labo):
        nom = input("nom")
        bureau = input("nouveau bureau")
        modifier_bureau (labo,nom,bureau)

    def gerer_nom(labo = labo):
        nom = input("nom")
        modifier_nom(labo, nom)
        
    def information_membre(labo):
        nom = input("nom")
        afficher_membre (labo, nom)

    def information_bureau(labo):
        nom = input("nom")
        afficher_bureau (labo, nom)

    def all_personnels(labo = labo):
        lister_labo(labo)
    
    def occupation_des_bureaux(labo=labo):
        bureau_list = occupation_bureaux(labo)
        for bureau, nom_labo in bureau_list.items():
            print(bureau, ":")
            for nom in nom_labo :
                print("-",nom)

    

    # Construire le menu
    mp = Menu()
    ajouter(mp, "Enregistrer arrivée", gerer_arrivee)
    ajouter(mp, "Enregistrer départ", gerer_depart)
    ajouter(mp, "modifier le nom d'une personne", gerer_nom)
    ajouter(mp, "Afficher l'ensemble du personnel", all_personnels)
    ajouter(mp, "Afficher l'ensemble des bureaux avec leurs personnels", occupation_des_bureaux)

    choix = None
    while choix != 0:
        afficher_menu(mp)
        choix = choix_utilisateur(mp)
        traiter(mp, choix)
        print('labo', labo)
        print()

main()