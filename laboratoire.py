"""Les opérations sur un laboratoire"""

class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass



def Laboratoire ():
    return {}


def enregistrer_arrivee(labo, nom, bureau):
    if nom in labo :
        raise PresentException
    labo[nom] = bureau

def enregistrer_depart(labo,nom):
    if nom not in labo: 
        raise AbsentException
    del labo[nom]

def modifier_bureau(labo, nom,bureau):
    if nom in labo : 
        labo[nom] = bureau 
    else : 
        raise AbsentException

def modifier_nom(labo,nom): 
    if nom in labo : 
        nouveau_nom = input("Nouveau nom : ")
        labo[nouveau_nom] = labo.pop(nom)
    

def afficher_membre(labo,nom):
    if nom in labo :
        print (nom, "est membre du laboratoire")
    else : 
        print (nom,"n'est pas membre du laboratoire")

def afficher_bureau(labo,nom):
    if nom in labo :
        print(f"Le bureau de {nom} est : {labo.get(nom)} ")
    

def lister_labo(labo):
    print("Nom : \t Bureau :")
    for cle, valeur in labo.items():
        print(cle,"\t", valeur)


def occupation_bureaux(labo): #exercice 1.2 
    bureau_list = {}
    for nom, bureau in labo.items() :
        bureau_list.setdefault(bureau, set())
        bureau_list[bureau].add(nom)
        sorted(bureau_list) #ordre lexicographique

    return bureau_list









