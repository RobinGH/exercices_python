def Menu ():
    return []

def ajouter (menu, texte, action):
    menu.append((texte, action)) #Mettre des fonctions dans des arguments


def afficher_menu(menu):
    for num, (texte,_) in enumerate(menu, 1): #afficher les premieres lignes 
        print(f"{num:2} - {texte}")
    print(f"{0:2} - Quitter")


def choix_utilisateur(menu):
    return int(input("Votre choix : "))


def traiter(menu,choix):
    if choix != 0 : 
        _, action = menu[choix -1]  
        action()
